#/bin/bash

if [ "$0" = "./build32" ] ; then
	echo Building 32-bit kernel
	export ARCH="arm"
	export CROSS_COMPILE="/home/sergeym/work/gcc-7.5/bin/arm-linux-gnueabihf-"
else
	echo Building 64-bit kernel
	export ARCH="arm64"
	#export CROSS_COMPILE="/home/sergeym/work/gcc-7.5-aarch64/bin/aarch64-linux-gnu-"
	export CROSS_COMPILE="/home/sergeym/work/ubertc-aarch64-4.9-kernel-gcc/bin/aarch64-linux-android-"
fi
#export CROSS_COMPILE="/usr/bin/arm-linux-gnueabihf-"
export INSTALL_MOD_PATH="./modules"
export INSTALL_MOD_PATH=./modules
export INSTALL_MOD_STRIP=1
export OPTS="-j6"

# if sourced, just set env vars, don't build
IF_SRC=`echo $0 | sed -n '/bin.*sh/p'`
if [ "x$IF_SRC" = "x" ] ; then
	# just install modules and kernel in local "modules" dir
	if [ "x$1" = "xmod" ] ; then
		make $OPTS ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE modules_install

		VER=`sed -n 's/.*UTS_RELEASE.*"\(.*\)".*/\1/p' include/generated/utsrelease.h`
		rm modules/lib/modules/$VER/build
		rm modules/lib/modules/$VER/source

		# copy the kernel
		cp arch/arm/boot/zImage modules/vmlinuz-$VER

		find modules/lib/modules/4.9.112-221931635/kernel/drivers/ -type f -exec ls -l {} \;

	else
		# build the kernel and modules
		if [ "x$1" != "x0" ] ; then
			echo "Continue with full build? Y | N"
			read answer
			if [ "$answer" != "y" ] && [ "$answer" != "Y" ] ; then
				echo "Exiting..."
				exit 1
			fi
			make clean
			# adjust local version: julian day as a suffix
			sed -i 's/\(CONFIG_LOCALVERSION=\).*$/\1"-'`date +%y%j`'"/' .config
			# to include build time
			#sed -i 's/\(CONFIG_LOCALVERSION=\).*$/\1"-'`date +%y%j%H%M`'"/' .config
		else
			echo BUILDING WITHOUT CLEANING
		fi

		make $OPTS ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE Image-dtb modules

		if [ $? -eq 0 ] ; then
			ls -l drivers/net/wireless/prima/wlan.o
			${CROSS_COMPILE}strip --strip-debug drivers/net/wireless/prima/wlan.ko
			ls -l drivers/net/wireless/prima/wlan.ko
			ls -l arch/$ARCH/boot/*Image-dtb
			md5sum drivers/net/wireless/prima/wlan.ko
			md5sum arch/$ARCH/boot/*Image-dtb
		fi
	fi
fi


